# James Woods

# Usage :
# c:\windows\debug\CheckBuildWithArgs.ps1 -datacenter "djvjkckv(London)fgd" -instancetype "nano : 1vCPU 1GB" -user "cv" -disk "30" -letter "c:" -autosys "y"
# Nov 1st, 2019



# Pass in args
param 
( 
        [Parameter(Mandatory=$True)]
        [string]$datacenter,
        [string]$instancetype,
        [string]$user,
        [string]$disk,
        [string]$letter,
        [string]$autosys,
        [string]$netbackup
)

# Log file to check
$file = Get-Content "C:\Windows\Debug\postbuild.log"



# Input parameters
#$domain = "EUROPE"
#$timezone = "GMT Standard Time"
#$site = "London"
#$cpus = 1
#$memory = "1GB"
$os = "Microsoft Windows Server 2012 R2 Standard"
#$letter = "d:"
#$disk = "30GB"
$services = @("Server","Task Sxxcheduler","Plug and Play","Virtual Disk")
#$user = "takako"

# Split instancetype into cpu and memory strings
$cpu_ram=($instancetype -split ":")[1]
$memorystr=($cpu_ram -split " ")[2]
$cpustr=($cpu_ram -split " ")[1]
$cpus=$cpustr.Substring(0,$cpustr.length-4)
$memory=$memorystr.Substring(0,$memorystr.length-2)


# Update services array if necessary
If ($autosys -ne "")
    {
    $services = $services += 'Print Spooler'
    }
If ($netbackup -ne "")
    {
    $services = $services += 'netbackup'
    }

# Get domain, timezone and site dependent on datacenter
    
    switch -Wildcard ( $datacenter )
    {
        '*London*'
        {
        $domain = "EUROPE"
        $timezone = "GMT Standard Time"
        $site = "London"
        }
        '*Tokyo*'
        {
        $domain = "JAPAN"
        $timezone = "Japan Standard Time"
        $site = "Tokyo"
        }
        default
        {
        $domain = "EUROPE"
        $timezone = "GMT Standard Time"
        $site = "London"
        }
    }

# Hashtable of strings to check exist
$checkstrings = @{
domaintext = "SERVICE: Domain selection was DEFAULT, Domain is $domain"
sitetext = "SERVICE-DEPLOY-TMPLTPOSTBLD: AD Site has been determined to be $site"
missingtext = "hdfkjsdhfkjsdhf"
timezonetext = "SERVICE-CONFIGURE: Time Zone set as $timezone"
}

# Hashtable of results
$results = @{}

# Loop through hashtable checking if in file. Add result to results hashtable
Foreach ($key in $checkstrings.keys)
    {
    $value = $checkstrings[$key]
    if ([bool]($file -like "*$value*"))
        { 
        write-debug "$value"
        $results.add($key, "TRUE")
        }
    else 
        {
        write-host -ForegroundColor Red "Cant find : $value"
        $results.add($key, "FALSE")
        }
    }




# Check memory
$wmiComp = Get-WmiObject -Class Win32_ComputerSystem 
$actualmemory = (([System.Math]::Round($wmiComp.TotalPhysicalMemory/1GB,2)).ToString()) 

If ($actualmemory -eq $memory)
    {
    $results.add("memory", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red "Actual memory = "$actualmemory "GB"
    $results.add("memory", "FALSE")
    }

# Check CPU
$actualcpus = $wmiComp.NumberOfProcessors 
If ($actualcpus -eq $cpus)
    {
    $results.add("cpus", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red "Actual CPUs = $actualcpus"
    $results.add("cpus", "FALSE")
    }

# Check O/S
$wmiComp = Get-WmiObject -Class Win32_OperatingSystem 
$actualos = $wmiComp.Caption 
If ($actualos -eq $os)
    {
    $results.add("os", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red $wmiComp.Caption
    $results.add("os", "FALSE")
    }

# Check extra disk

If ($letter -ne ""){

$wmiDisk = Get-WmiObject -Class Win32_LogicalDisk | where{$_.Caption -eq "$letter"}
$actualsize = (([System.Math]::Round($wmiDisk.size/1GB)).ToString()) 
If ($actualsize -eq $disk)
    {
    $results.add("disk", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red "Actual disk space is "$actualsize"GB"
    $results.add("disk", "FALSE")
    }
}

# Check activated
$licensekey = (Get-WmiObject -Class SoftwareLicensingProduct| Where {$_.PartialProductKey})
[string]$license = $licensekey.licensestatus
If ($license -eq '1') 
    {
    $results.add("activated", "TRUE")
    }
else
    {
    write-host -ForegroundColor Red "activated"
    $results.add("activated", "FALSE")
    }
    
# Check services
Foreach ($service in $services)
    {
    try
        {
        $servicedetail = get-service $service -ErrorAction Stop
        If ($servicedetail.status -eq "running" -and $servicedetail.StartType -eq "auto") 
            {
            $results.add($service, "TRUE")
            }
        else
            {
            write-host -ForegroundColor Red "$service   $($servicedetail.status)    $($servicedetail.StartType)"
            $results.add($service, "FALSE")
            }
        }
    catch
        {
        write-host -ForegroundColor Red "Can't find : $service"
        $results.add($service, "FALSE")
        }    
    }  

# Check user in Remote Desktop Users
$UserExists = [bool](Get-WmiObject -Class Win32_GroupUser | Where-Object {$_.GroupComponent -match "Remote Desktop Users" -and
$_.PartComponent.Contains($user)})
If ($UserExists)
    {
    $results.add("user", "TRUE")
    }
else
    {
    write-host -ForegroundColor Red "User not in Remote Desktop Users group : $user"
    $results.add("user", "FALSE")
    }



$results 

$results | out-string | set-content c:\windows\debug\postbuild-check.txt





